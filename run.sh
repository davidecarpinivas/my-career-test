#!/bin/bash

. config.sh

cd $REPORT_DIR

nodemon ./ -p $REPORT_PORT --dataset_url $SURVEY_HOST:$SURVEY_PORT &

cd $PDFREPORT_DIR

nodemon dist/server/server.js -p $PDFREPORT_PORT &

cd $SURVEY_DIR

node ./ -i examples/my-career-status/index.srv -d \
  mongodb://localhost/car-s -p $SURVEY_PORT -s "saml" \
  -l --autoresponse \
  -k "a087d9a5-711b-4056-a841-7ae598a3acf5" -x \
  --urlreport $REPORT_HOST:$REPORT_PORT \
  --urlpdfgenerator $PDFREPORT_HOST:$PDFREPORT_PORT \
  --pdfname $PDFNAME
