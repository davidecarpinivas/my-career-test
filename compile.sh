#!/bin/bash
. config.sh

cd $PDFREPORT_DIR

webpack -w &

cd $SURVEY_DIR

webpack -w &

cd $REPORT_DIR

yarn compile
